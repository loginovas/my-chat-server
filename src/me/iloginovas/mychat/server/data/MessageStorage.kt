package me.iloginovas.mychat.server.data

import me.iloginovas.mychat.common.model.Message
import me.iloginovas.mychat.server.data.db.MessageEntity
import me.iloginovas.mychat.server.data.db.MessageTable
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll

interface MessageStorage : TransactionalStorage {
    fun addMessages(messages: List<Message>)

    fun getLatestMessage(): Message?

    fun getLatestMessages(maxNumber: Int): List<Message>
    fun getMessagesNewerThan(messageId: Int, maxNumber: Int): List<Message>
    fun getMessagesOlderThan(messageId: Int, maxNumber: Int): List<Message>
}

class MessageStorageImpl : MessageStorage {

    override fun addMessages(messages: List<Message>) {
        messages.forEach { msg ->
            MessageEntity.new(msg.id) {
                senderId = msg.senderId
                text = msg.text
                sentAt = msg.sentAt
            }
        }
    }

    override fun getLatestMessages(maxNumber: Int): List<Message> {
        return MessageTable.selectAll()
            .orderBy(MessageTable.id, SortOrder.DESC)
            .limit(maxNumber)
            .map { it.toMessage() }
    }

    override fun getMessagesNewerThan(messageId: Int, maxNumber: Int): List<Message> {
        return MessageTable
            .select { MessageTable.id greater messageId }
            .orderBy(MessageTable.id, SortOrder.ASC)
            .limit(maxNumber)
            .map { it.toMessage() }
    }

    override fun getMessagesOlderThan(messageId: Int, maxNumber: Int): List<Message> {
        return MessageTable
            .select { MessageTable.id less messageId }
            .orderBy(MessageTable.id, SortOrder.DESC)
            .limit(maxNumber)
            .map { it.toMessage() }
    }

    override fun getLatestMessage(): Message? {
        val resultRow = MessageTable.selectAll()
            .orderBy(MessageTable.id, SortOrder.DESC)
            .limit(1)
            .firstOrNull() ?: return null

        return resultRow.toMessage()
    }
}

private fun ResultRow.toMessage() = Message(
    id = this[MessageTable.id].value,
    text = this[MessageTable.text],
    senderId = this[MessageTable.senderId],
    sentAt = this[MessageTable.sentAt]
)