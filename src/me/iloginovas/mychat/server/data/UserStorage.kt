package me.iloginovas.mychat.server.data

import me.iloginovas.mychat.server.data.db.UserEntity
import me.iloginovas.mychat.server.data.db.UserEntity.Companion.normalizePassword
import me.iloginovas.mychat.server.data.db.UserEntity.Companion.normalizeUsername
import me.iloginovas.mychat.server.data.db.UserTable
import me.iloginovas.mychat.server.model.NewUser
import me.iloginovas.mychat.server.model.User

interface UserStorage : TransactionalStorage {
    fun findById(id: Int): User?
    fun findByIds(ids: List<Int>): List<User>
    fun findByName(username: String): User?

    fun createUser(newUser: NewUser): User
}

class UserStorageImpl : UserStorage {
    private val chunkSize = 64

    override fun findById(id: Int): User? =
        UserEntity.findById(id)?.toUser()

    override fun findByIds(ids: List<Int>): List<User> {
        if (ids.size <= chunkSize)
            return UserEntity.find { UserTable.id inList ids }
                .map(UserEntity::toUser)
        
        return ids.asSequence()
            .chunked(chunkSize)
            .flatMapTo(ArrayList(ids.size)) {
                UserEntity.find { UserTable.id inList ids }
                    .map(UserEntity::toUser)
            }
    }

    override fun findByName(username: String): User? =
        normalizeUsername(username).let { name ->
            UserEntity.find { UserTable.name eq name }
                .limit(1)
                .firstOrNull()?.toUser()
        }

    override fun createUser(newUser: NewUser): User {
        val userEntity = UserEntity.new {
            name = normalizeUsername(newUser.name)
            password = normalizePassword(newUser.password)
        }
        return userEntity.toUser()
    }
}

private fun UserEntity.toUser(): User =
    User(id.value, name, password)