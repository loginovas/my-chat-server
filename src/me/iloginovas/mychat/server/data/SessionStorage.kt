package me.iloginovas.mychat.server.data

import me.iloginovas.mychat.server.data.db.SessionEntity
import me.iloginovas.mychat.server.model.ChatSession
import java.util.*

interface SessionStorage : TransactionalStorage {

    fun createSession(userId: Int): ChatSession

    fun deleteSession(sessionId: String)

    fun findById(sessionId: String): ChatSession?
}

class SessionStorageImpl : SessionStorage {

    override fun createSession(userId: Int): ChatSession {
        val sessionEntity = SessionEntity.new(UUID.randomUUID()) {
            this.userId = userId
        }
        return sessionEntity.toModel()
    }

    override fun deleteSession(sessionId: String) {
        SessionEntity.findById(UUID.fromString(sessionId))?.delete()
    }

    override fun findById(sessionId: String): ChatSession? =
        SessionEntity.findById(UUID.fromString(sessionId))
            ?.toModel()

}

private fun SessionEntity.toModel(): ChatSession =
    ChatSession(id.value.toString(), userId)