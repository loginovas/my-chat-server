package me.iloginovas.mychat.server.data

/**
 * All storage's methods must be called from within a transaction, otherwise an error occurs.
 * You can do it like this:
 *
 * ```
 * val transactionManager: TransactionManager
 * val storage: SomeStorage
 * transactionManager.readTransaction {
 *     storage.findByName("some_name")
 * }
 * ```
 */
interface TransactionalStorage