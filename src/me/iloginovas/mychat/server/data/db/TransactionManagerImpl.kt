package me.iloginovas.mychat.server.data.db

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExecutorCoroutineDispatcher
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.withContext
import me.iloginovas.mychat.server.data.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import java.util.concurrent.Executors
import kotlin.concurrent.thread

private const val writeThreadName = "db-writeThread"

class TransactionManagerImpl : TransactionManager {

    private val log = LoggerFactory.getLogger(TransactionManagerImpl::class.java)

    private val writeDispatcher: ExecutorCoroutineDispatcher

    init {
        writeDispatcher = Executors.newSingleThreadExecutor {
            Thread(it, writeThreadName)
        }.asCoroutineDispatcher().also { closeOnJvmShutdown(it) }
    }

    override suspend fun <T> readTransaction(readFun: () -> T): T =
        withContext(Dispatchers.IO) {
            transaction { readFun() }
        }

    override suspend fun <T> writeTransaction(writeFun: () -> T): T =
        withContext(writeDispatcher) {
            transaction { writeFun() }
        }

    private fun closeOnJvmShutdown(dispatcher: ExecutorCoroutineDispatcher) {
        val name = "shutdownHook"
        Runtime.getRuntime().addShutdownHook(
            thread(start = false, name = name) {
                log.info("$name: shutdown start")
                dispatcher.close()
            })
    }
}