package me.iloginovas.mychat.server.data.db

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.sqlite.SQLiteDataSource
import java.util.concurrent.atomic.AtomicBoolean

const val dbFilename = "chat.db"

object ChatDatabase {
    private val initialized = AtomicBoolean(false)

    fun init() {
        if (initialized.get()) return

        synchronized(this) {
            if (initialized.get()) return
            initDatabase()
            initialized.set(true)
        }
    }
}

private fun initDatabase() {
    val ds = SQLiteDataSource().apply {
        url = "jdbc:sqlite:$dbFilename"
    }
    Database.connect(ds)

    transaction {
        SchemaUtils.create(UserTable, SessionTable, MessageTable)

        val query: Query = UserTable.selectAll().limit(1)
        val resultRow: ResultRow? = query.firstOrNull()
        if (resultRow == null) {
            val adminId = UserTable.insertAndGetId {
                it[name] = "admin"
                it[password] = "admin"
            }
            exposedLogger.debug("User admin created $adminId")
        }
    }
}