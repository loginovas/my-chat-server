package me.iloginovas.mychat.server.data.db

import me.iloginovas.mychat.server.data.db.MessageTable.id
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

private const val maxMessageLength = 200

/**
 * Values of [id] grow in order from old to new which means that
 * Message[id=6] is newer than Message[id=5].
 *
 * In the current implementation, we require the [id] to be a sequence.
 * That is, the first value is 1 and each next value is greater by one (1, 2, 3,...).
 * For simplicity of implementation, we will not allow deleting messages for now.
 **/
object MessageTable : IntIdTable("message") {

    val text = varchar("text", maxMessageLength)

    val senderId = integer("sender_id")
        .references(UserTable.id)

    /** Stored as seconds since epoch */
    val sentAt = long("sentAt").index()
}

class MessageEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<MessageEntity>(MessageTable)

    var text: String by MessageTable.text
    var senderId: Int by MessageTable.senderId
    var sentAt: Long by MessageTable.sentAt
}