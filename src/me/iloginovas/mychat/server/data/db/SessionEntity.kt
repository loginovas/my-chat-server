package me.iloginovas.mychat.server.data.db

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.*

object SessionTable : UUIDTable("chat_session") {
    val userId = integer("user_id")
        .index()
        .references(UserTable.id)
}

class SessionEntity(id: EntityID<UUID>) : UUIDEntity(id) {
    var userId: Int by SessionTable.userId

    companion object : UUIDEntityClass<SessionEntity>(SessionTable)
}