package me.iloginovas.mychat.server.data.db

import me.iloginovas.mychat.common.validation.PasswordSpec
import me.iloginovas.mychat.common.validation.UsernameSpec
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object UserTable : IntIdTable("person") {

    val name = varchar("name", UsernameSpec.maxLength)
        .uniqueIndex()

    val password = varchar("password", PasswordSpec.maxLength)
}

class UserEntity(id: EntityID<Int>) : IntEntity(id) {
    var name: String by UserTable.name
    var password: String by UserTable.password

    companion object : IntEntityClass<UserEntity>(UserTable) {

        fun normalizeUsername(username: String): String =
            username.trim().toLowerCase()

        fun normalizePassword(password: String): String = password.trim()
    }
}