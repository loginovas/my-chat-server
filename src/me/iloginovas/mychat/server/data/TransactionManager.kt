package me.iloginovas.mychat.server.data

/**
 * All storage's methods must be called from within a transaction, otherwise an error occurs.
 * @see TransactionalStorage
 */
interface TransactionManager {

    /**
     * With shared lock (read lock).
     * Multiple read transactions can be running at the same time and
     * no read transaction can be running simultaneously with write transaction.
     */
    suspend fun <T> readTransaction(readFun: () -> T): T

    /**
     * With exclusive lock.
     * Only one write transaction can be running at the same time and
     * no read transaction can be running simultaneously with the write transaction.
     */
    suspend fun <T> writeTransaction(writeFun: () -> T): T
}

interface TransactionSupport {
    val transactionManager: TransactionManager

    suspend fun <T> readTransaction(readFun: () -> T): T =
        transactionManager.readTransaction(readFun)

    suspend fun <T> writeTransaction(writeFun: () -> T): T =
        transactionManager.writeTransaction(writeFun)
}