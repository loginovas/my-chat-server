package me.iloginovas.mychat.server.service

import me.iloginovas.mychat.common.model.Message
import me.iloginovas.mychat.server.model.NewMessage
import me.iloginovas.mychat.server.model.User

object ClientEvents {

    /** Means that client wants to send [message] */
    data class SendMessage(val message: NewMessage) : ClientEvent
}

object ServerEvents {
    /**
     * Means that there are the new messages [messages] in chat that the server
     * wants to send to the connected client.
     */
    data class NewMessages(val messages: List<Message>) : ServerEvent

    /**
     * Means that the client was disconnected from chat for some reason, for example,
     * because the user changed the password or removed the account.
     */
    data class ClientIsDisconnected(val cause: String?) : ServerEvent
}

object RequestEvents {

    class GetUsers(
        override val params: RequestResponseEvent.Params,
        val userIds: List<Int>
    ) : RequestEvent<ResponseEvents.GetUsers>

    sealed class GetMessages : RequestEvent<ResponseEvents.GetMessages> {
        class Latest(
            override val params: RequestResponseEvent.Params,
            val preferredMaxCount: Int
        ) : GetMessages()

        class NewerThan(
            override val params: RequestResponseEvent.Params,
            val messageId: Int, val preferredMaxCount: Int
        ) : GetMessages()

        class OlderThan(
            override val params: RequestResponseEvent.Params,
            val messageId: Int, val preferredMaxCount: Int
        ) : GetMessages()
    }
}

object ResponseEvents {

    class GetUsers(
        override val params: RequestResponseEvent.Params,
        val users: List<User>
    ) : ResponseEvent

    class GetMessages(
        override val params: RequestResponseEvent.Params,
        val messages: List<Message>
    ) : ResponseEvent
}