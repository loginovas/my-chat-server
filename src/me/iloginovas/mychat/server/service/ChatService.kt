package me.iloginovas.mychat.server.service

import io.ktor.application.*
import kotlinx.coroutines.flow.Flow
import me.iloginovas.mychat.server.model.NewMessage
import java.util.*

/**
 * Supports communication between client and server through asynchronous event exchange.
 * There are two types of event:
 * - Simple events like [ClientEvents.SendMessage].
 * - Request-response style events. It is like HTTP request-response.
 *
 * An example of Request-Response communication:
 * - The client sends a [RequestEvent] with the specified id [RequestEvent.id].
 * - The server responds to this event with a [ResponseEvent] with the same id that was specified in the request.
 *
 * Important to know the [ResponseEvent] is sent to the same connection from which the request was sent
 * (see [ConnectionInfo.connectionId] and [RequestResponseEvent.params.connectionId]).
 */
interface ChatService {

    fun start(applicationScope: Application)

    fun connectClient(
        connectionId: UUID,
        sessionId: String,
        clientEventFlow: Flow<ClientEvent>
    ): Flow<ServerEvent>

    suspend fun sendMessage(message: NewMessage)
}

interface ChatEvent

/** The events that the client sends to the server */
interface ClientEvent : ChatEvent

/** The events that the server sends to the client */
interface ServerEvent : ChatEvent

interface RequestResponseEvent : ChatEvent {
    val params: Params

    val id: UUID get() = params.id
    val connectionId: UUID get() = params.connectionId

    class Params(
        val id: UUID = UUID.randomUUID(),
        val connectionId: UUID
    )
}

interface RequestEvent<T : ResponseEvent> :
    RequestResponseEvent, ClientEvent

interface ResponseEvent :
    RequestResponseEvent, ServerEvent

open class ErrorResponseEvent(
    override val params: RequestResponseEvent.Params
) : ResponseEvent