package me.iloginovas.mychat.server.service

import me.iloginovas.mychat.common.Result
import me.iloginovas.mychat.common.SuccessOrNot
import me.iloginovas.mychat.common.validation.PasswordValidator
import me.iloginovas.mychat.common.validation.UsernameValidator
import me.iloginovas.mychat.server.data.TransactionManager
import me.iloginovas.mychat.server.data.TransactionSupport
import me.iloginovas.mychat.server.data.UserStorage
import me.iloginovas.mychat.server.model.Credentials
import me.iloginovas.mychat.server.model.NewUser
import me.iloginovas.mychat.server.model.User

interface UserService {

    suspend fun authenticate(credentials: Credentials): Result<User>

    suspend fun registerUser(credentials: Credentials): Result<User>

    suspend fun findUserById(id: Int): User?

    suspend fun findUserByName(username: String): User?
}

class UserServiceImpl(
    private val userStorage: UserStorage,
    override val transactionManager: TransactionManager

) : UserService, TransactionSupport {

    override suspend fun authenticate(credentials: Credentials): Result<User> {
        val (username, password) = credentials

        val user = findUserByName(username)
            ?: return Result.Failure("User with specified name not found")

        return when (user.password == password.trim()) {
            true -> Result.Success(user)
            else -> Result.Failure("Wrong password")
        }
    }

    override suspend fun registerUser(credentials: Credentials): Result<User> {
        val (username, password) = credentials

        UsernameValidator.validate(username).let {
            if (it is SuccessOrNot.Failure)
                return Result.Failure("Invalid username: ${it.error.errorMsg}")
        }
        PasswordValidator.validate(password).let {
            if (it is SuccessOrNot.Failure)
                return Result.Failure("Invalid password: ${it.error.errorMsg}")
        }

        fun userAlreadyExistsError(name: String): Result.Failure =
            Result.Failure("User '${name}' already exists")

        findUserByName(username)?.let {
            return userAlreadyExistsError(it.name)
        }

        return writeTransaction {
            userStorage.findByName(username)?.let {
                return@writeTransaction userAlreadyExistsError(it.name)
            }
            val newUser = userStorage.createUser(NewUser(username, password))
            Result.Success(newUser)
        }
    }

    override suspend fun findUserById(id: Int): User? =
        readTransaction {
            userStorage.findById(id)
        }

    override suspend fun findUserByName(username: String): User? =
        readTransaction {
            userStorage.findByName(username)
        }
}