package me.iloginovas.mychat.server.service

import me.iloginovas.mychat.common.Result
import me.iloginovas.mychat.common.toResult
import me.iloginovas.mychat.server.data.SessionStorage
import me.iloginovas.mychat.server.data.TransactionManager
import me.iloginovas.mychat.server.data.TransactionSupport
import me.iloginovas.mychat.server.data.UserStorage
import me.iloginovas.mychat.server.model.ChatSession
import me.iloginovas.mychat.server.model.User

interface SessionManager {

    suspend fun newSession(user: User): Result<ChatSession>

    suspend fun getSessionById(sessionId: String): Result<ChatSession>

    suspend fun dropSession(sessionId: String)
}

class SessionManagerImpl(
    override val transactionManager: TransactionManager,
    private val userStorage: UserStorage,
    private val sessionStorage: SessionStorage
) : SessionManager, TransactionSupport {

    override suspend fun newSession(user: User): Result<ChatSession> =
        writeTransaction {
            if (userStorage.findById(user.id) == null)
                Result.Failure("User not found")
            else
                Result.Success(sessionStorage.createSession(user.id))
        }


    override suspend fun getSessionById(sessionId: String): Result<ChatSession> =
        readTransaction {
            sessionStorage.findById(sessionId).toResult()
        }

    override suspend fun dropSession(sessionId: String) =
        writeTransaction {
            sessionStorage.deleteSession(sessionId)
        }
}

