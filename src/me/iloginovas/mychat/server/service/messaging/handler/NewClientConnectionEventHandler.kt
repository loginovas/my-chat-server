package me.iloginovas.mychat.server.service.messaging.handler

import me.iloginovas.mychat.server.data.MessageStorage
import me.iloginovas.mychat.server.data.TransactionManager
import me.iloginovas.mychat.server.data.TransactionSupport
import me.iloginovas.mychat.common.model.Message
import me.iloginovas.mychat.server.service.ServerEvents
import me.iloginovas.mychat.server.service.messaging.ChatConnection
import me.iloginovas.mychat.server.service.messaging.ChatEngineEvent.NewClientConnection
import me.iloginovas.mychat.server.service.messaging.ConnectionsHolder
import org.slf4j.LoggerFactory

private const val MAX_NUMBER_OF_RECENT_MESSAGES = 50

class EventHandlerNewClientConnection(
    private val connectionsHolder: ConnectionsHolder,
    private val messageStorage: MessageStorage,
    override val transactionManager: TransactionManager
) : EventHandler<NewClientConnection>, TransactionSupport {

    private val collector = Collector(this::handle)
    private val log = LoggerFactory.getLogger(this::class.java)

    override suspend fun startHandling(initCapacity: Int?): EventCollector<NewClientConnection> {
        collector.prepareBeforeCollecting(initCapacity)
        return collector
    }

    private suspend fun handle(collector: Collector) {
        val connections: List<ChatConnection> = collector.connections!!
        if (connections.isEmpty()) return

        connections.forEach { connection ->
            connectionsHolder.putConnection(connection)
            log.info("New connection added $connection")
        }

        val recentMessages: List<Message> = readTransaction {
            messageStorage.getLatestMessages(MAX_NUMBER_OF_RECENT_MESSAGES)
        }
        if (recentMessages.isEmpty()) return

        val messageToClient = ServerEvents.NewMessages(recentMessages)
        connections.forEach { connection ->
            connection.serverEvents.send(messageToClient)
        }
    }

    private class Collector(
        private val handleFun: suspend (Collector) -> Unit
    ) : EventCollector<NewClientConnection> {

        var connections: ArrayList<ChatConnection>? = null

        fun prepareBeforeCollecting(initCapacity: Int?) {
            connections = if (initCapacity == null) ArrayList() else ArrayList(initCapacity)
        }

        override suspend fun handle() {
            handleFun(this)
            connections = null
        }

        override fun add(event: NewClientConnection) {
            connections!!.add(event.connection)
        }
    }
}