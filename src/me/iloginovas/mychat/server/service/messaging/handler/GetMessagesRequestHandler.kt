package me.iloginovas.mychat.server.service.messaging.handler

import me.iloginovas.mychat.common.model.Message
import me.iloginovas.mychat.server.data.MessageStorage
import me.iloginovas.mychat.server.data.TransactionManager
import me.iloginovas.mychat.server.data.TransactionSupport
import me.iloginovas.mychat.server.service.ErrorResponseEvent
import me.iloginovas.mychat.server.service.RequestEvents
import me.iloginovas.mychat.server.service.ResponseEvents
import me.iloginovas.mychat.server.service.messaging.ConnectionsHolder
import org.slf4j.LoggerFactory

class GetMessagesRequestHandler(
    private val connectionsHolder: ConnectionsHolder,
    private val messageStorage: MessageStorage,
    override val transactionManager: TransactionManager

) : EventHandler<RequestEvents.GetMessages>, EventCollector<RequestEvents.GetMessages>, TransactionSupport {
    private val log = LoggerFactory.getLogger(this.javaClass)

    private val maxSize = 100
    private var requests: ArrayList<RequestEvents.GetMessages>? = null

    override suspend fun startHandling(initCapacity: Int?): EventCollector<RequestEvents.GetMessages> {
        requests = if (initCapacity == null) ArrayList() else ArrayList(initCapacity)
        return this
    }

    override fun add(event: RequestEvents.GetMessages) {
        requests!!.add(event)
    }

    override suspend fun handle() {
        val requests = requests!!
        this.requests = null

        requests.forEach { request ->
            val response = try {
                val messages = getMessages(request)
                ResponseEvents.GetMessages(request.params, messages)
            } catch (e: Throwable) {
                log.error("Cannot fetch messages", e)
                ErrorResponseEvent(request.params)
            }

            connectionsHolder.forConnection(response.connectionId) {
                try {
                    it.serverEvents.send(response)
                } catch (e: Throwable) {
                    log.error("Cannot send server event", e)
                }
            }
        }
    }

    private suspend fun getMessages(request: RequestEvents.GetMessages): List<Message> =
        readTransaction {
            when (request) {
                is RequestEvents.GetMessages.Latest ->
                    messageStorage.getLatestMessages(
                        minOf(request.preferredMaxCount, maxSize)
                    )
                is RequestEvents.GetMessages.NewerThan ->
                    messageStorage.getMessagesNewerThan(
                        request.messageId, minOf(request.preferredMaxCount, maxSize)
                    )
                is RequestEvents.GetMessages.OlderThan ->
                    messageStorage.getMessagesOlderThan(
                        request.messageId, minOf(request.preferredMaxCount, maxSize)
                    )
            }
        }
}