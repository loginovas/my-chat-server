package me.iloginovas.mychat.server.service.messaging.handler

import me.iloginovas.mychat.server.data.TransactionManager
import me.iloginovas.mychat.server.data.TransactionSupport
import me.iloginovas.mychat.server.data.UserStorage
import me.iloginovas.mychat.server.model.User
import me.iloginovas.mychat.server.service.RequestEvents
import me.iloginovas.mychat.server.service.ResponseEvents
import me.iloginovas.mychat.server.service.messaging.ConnectionsHolder
import org.slf4j.LoggerFactory
import kotlin.collections.set

class GetUserRequestHandler(
    private val connectionsHolder: ConnectionsHolder,
    private val userStorage: UserStorage,
    override val transactionManager: TransactionManager

) : EventHandler<RequestEvents.GetUsers>, EventCollector<RequestEvents.GetUsers>, TransactionSupport {
    private val log = LoggerFactory.getLogger(this.javaClass)

    private var requests: ArrayList<RequestEvents.GetUsers>? = null

    override suspend fun startHandling(initCapacity: Int?): EventCollector<RequestEvents.GetUsers> {
        requests = if (initCapacity == null) ArrayList() else ArrayList(initCapacity)
        return this
    }

    override fun add(event: RequestEvents.GetUsers) {
        requests!!.add(event)
    }

    override suspend fun handle() {
        val requests = requests!!
        this.requests = null

        val userById = HashMap<Int, User>()

        requests.asSequence()
            .flatMap { it.userIds }
            .distinct().chunked(100)
            .forEach { ids: List<Int> ->
                getUsersByIds(ids).forEach {
                    userById[it.id] = it
                }
            }

        requests.forEach { request ->
            val users = request.userIds.mapNotNull { userById[it] }
            val response = ResponseEvents.GetUsers(request.params, users)
            connectionsHolder.forConnection(response.connectionId) {
                try {
                    it.serverEvents.send(response)
                } catch (e: Throwable) {
                    log.error("Cannot send server event", e)
                }
            }
        }
    }

    private suspend fun getUsersByIds(ids: List<Int>): List<User> =
        try {
            readTransaction { userStorage.findByIds(ids) }
        } catch (e: Throwable) {
            log.error("Cannot fetch users by ids", e)
            emptyList()
        }

}