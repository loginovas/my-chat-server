package me.iloginovas.mychat.server.service.messaging.handler

import me.iloginovas.mychat.server.service.messaging.ChatConnection
import me.iloginovas.mychat.server.service.messaging.ChatEngineEvent.ClientConnectionClosed
import me.iloginovas.mychat.server.service.messaging.ConnectionsHolder
import org.slf4j.LoggerFactory

class ClientConnectionClosedEventHandler(
    private val connectionsHolder: ConnectionsHolder
) : EventHandler<ClientConnectionClosed> {

    private val collector = Collector(this::handle)
    private val log = LoggerFactory.getLogger(this::class.java)

    private fun handle(connections: Collection<ChatConnection>) {
        connections.forEach { connection ->
            try {
                connectionsHolder.removeConnection(connection)
                log.info("Connection was closed $connection")
            } catch (e: Throwable) {
                log.error("Cannot remove connection $connection", e)
            }
        }
    }

    override suspend fun startHandling(initCapacity: Int?): EventCollector<ClientConnectionClosed> {
        collector.prepareBeforeCollecting(initCapacity)
        return collector
    }

    private class Collector(
        private val handleFun: suspend (connections: Collection<ChatConnection>) -> Unit
    ) : EventCollector<ClientConnectionClosed> {

        var connections: MutableCollection<ChatConnection>? = null

        fun prepareBeforeCollecting(initCapacity: Int?) {
            connections = if (initCapacity == null) ArrayList() else ArrayList(initCapacity)
        }

        override fun add(event: ClientConnectionClosed) {
            connections!!.add(event.connection)
        }

        override suspend fun handle() {
            handleFun(connections!!)
            connections = null
        }
    }
}