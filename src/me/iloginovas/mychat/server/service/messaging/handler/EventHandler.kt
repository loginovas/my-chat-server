package me.iloginovas.mychat.server.service.messaging.handler

import me.iloginovas.mychat.server.service.ChatEvent

interface EventHandler<E : ChatEvent> {

    suspend fun init() {}

    /**
     * Usage:
     * - Before starting handling, you need to get [EventCollector] by calling [startHandling] method.
     * - Add events that you want to be handled by calling [EventCollector.add] method.
     * - Run handling the added events by calling [EventCollector.handle] method.
     *
     * You cannot use the same [EventCollector] twice, instead you need to get it again by [startHandling].
     */
    suspend fun startHandling(initCapacity: Int? = null): EventCollector<E>
}

interface EventCollector<E : ChatEvent> {

    fun add(event: E)

    /**
     * Process events that were added by [add].
     */
    suspend fun handle()
}

class EventHandlers {
    val map = LinkedHashMap<Class<*>, EventHandler<*>>()

    inline fun <reified T : ChatEvent> put(handler: EventHandler<T>) {
        put(T::class.java, handler)
    }

    fun <T : ChatEvent> put(eventType: Class<T>, handler: EventHandler<in T>) {
        map[eventType] = handler
    }

    inline fun <reified T : ChatEvent> get(): EventHandler<T> =
        get(T::class.java)

    @Suppress("UNCHECKED_CAST")
    fun <T : ChatEvent> get(eventType: Class<T>): EventHandler<T> {
        return map[eventType] as EventHandler<T>
    }

    inline fun forEach(action: (EventHandler<*>) -> Unit) {
        map.forEach { action(it.value) }
    }
}