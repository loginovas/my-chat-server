package me.iloginovas.mychat.server.service.messaging.handler

import me.iloginovas.mychat.server.data.MessageStorage
import me.iloginovas.mychat.server.data.TransactionManager
import me.iloginovas.mychat.server.data.TransactionSupport
import me.iloginovas.mychat.common.model.Message
import me.iloginovas.mychat.server.service.ClientEvents.SendMessage
import me.iloginovas.mychat.server.service.ServerEvents.NewMessages
import me.iloginovas.mychat.server.service.messaging.ChatConnection
import me.iloginovas.mychat.server.service.messaging.ConnectionsHolder
import org.slf4j.LoggerFactory
import java.time.Clock

class SendMessageEventHandler(
    private val connectionsHolder: ConnectionsHolder,
    private val messageStorage: MessageStorage,
    override val transactionManager: TransactionManager
) : EventHandler<SendMessage>, TransactionSupport {

    private val log = LoggerFactory.getLogger(this::class.java)
    private lateinit var collector: Collector

    override suspend fun init() {
        val lastMessage = readTransaction { messageStorage.getLatestMessage() }
        val lastMessageId = lastMessage?.id ?: 0
        collector = Collector(lastMessageId, this::handle)
    }

    override suspend fun startHandling(initCapacity: Int?): EventCollector<SendMessage> {
        collector.prepareBeforeCollecting(initCapacity)
        return collector
    }

    private suspend fun handle(collector: Collector) {
        val messages: List<Message> = collector.messages!!
        if (messages.isEmpty()) return

        writeTransaction {
            messageStorage.addMessages(messages)
        }

        val messageToClientEvent = NewMessages(messages)
        connectionsHolder.forEachConnection { connection: ChatConnection ->
            connection.serverEvents.send(messageToClientEvent)
        }
    }

    private class Collector(
        private var lastMessageId: Int,
        private val handleFun: suspend (Collector) -> Unit
    ) : EventCollector<SendMessage> {

        private val clock = Clock.systemUTC()
        private var sentTime: Long = -1

        var messages: ArrayList<Message>? = null

        suspend fun prepareBeforeCollecting(initCapacity: Int?) {
            messages = if (initCapacity == null) ArrayList() else ArrayList(initCapacity)
            sentTime = clock.instant().epochSecond
        }

        override suspend fun handle() {
            handleFun(this)
            messages = null
        }

        override fun add(event: SendMessage) {
            val message = event.message.let {
                Message(++lastMessageId, it.text, it.senderId, sentTime)
            }
            messages!!.add(message)
        }
    }
}