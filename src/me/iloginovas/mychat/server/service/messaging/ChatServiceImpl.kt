package me.iloginovas.mychat.server.service.messaging

import io.ktor.application.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.collect
import me.iloginovas.mychat.common.resultOrError
import me.iloginovas.mychat.server.data.MessageStorage
import me.iloginovas.mychat.server.data.TransactionManager
import me.iloginovas.mychat.server.data.TransactionSupport
import me.iloginovas.mychat.server.data.UserStorage
import me.iloginovas.mychat.server.model.NewMessage
import me.iloginovas.mychat.server.service.*
import org.slf4j.LoggerFactory
import java.util.*

class ChatServiceImpl(
    override val transactionManager: TransactionManager,
    private val userStorage: UserStorage,
    private val messageStorage: MessageStorage,
    private val sessionManager: SessionManager
) : ChatService, TransactionSupport {

    private val log = LoggerFactory.getLogger(this::class.java)
    private val chatEngine = ChatEngine(messageStorage, userStorage, transactionManager)

    override fun start(applicationScope: Application) {
        chatEngine.start(applicationScope)
    }

    @Suppress("EXPERIMENTAL_API_USAGE", "RemoveExplicitTypeArguments")
    override fun connectClient(
        connectionId: UUID,
        sessionId: String,
        clientEventFlow: Flow<ClientEvent>
    ): Flow<ServerEvent> =

        channelFlow<ServerEvent> {
            log.info("New connection request [sessionId=$sessionId]")

            val session = sessionManager.getSessionById(sessionId)
                .resultOrError { errorMsg ->
                    log.info("New connection error. Invalid session [sessionId=$sessionId]")
                    send(ServerEvents.ClientIsDisconnected(errorMsg))
                    return@channelFlow
                }

            val connection = ChatConnection(
                id = connectionId,
                sessionId = session.sessionId,
                userId = session.userId
            )

            chatEngine.postEvent(
                ChatEngineEvent.NewClientConnection(connection)
            )

            try {
                launch {
                    clientEventFlow.collect { clientEvent ->
                        log.debug(
                            "Client event [sessionId=$sessionId, userId=${session.userId}, " +
                                    "connectionId=${connection.id}, event=$clientEvent]"
                        )
                        chatEngine.postEvent(clientEvent)
                    }
                }

                while (true) {
                    try {
                        val serverEvent = connection.serverEvents.receive()
                        send(serverEvent)
                    } catch (e: Throwable) {
                        ensureActive()
                        log.info("Exception during serverEvent receiving $e")
                        continue
                    }
                }

            } finally {
                chatEngine.postEvent(
                    ChatEngineEvent.ClientConnectionClosed(connection)
                )
            }
        }

    override suspend fun sendMessage(message: NewMessage) {
        chatEngine.postEvent(ClientEvents.SendMessage(message))
    }
}