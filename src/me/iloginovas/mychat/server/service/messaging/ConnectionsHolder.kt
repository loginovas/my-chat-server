package me.iloginovas.mychat.server.service.messaging

import java.util.*
import kotlin.collections.HashMap

/**
 * Contains connections as map of (sessionId) -> (connections: List<ChatConnection>),
 * so it is allowed to have multiple connections for one session.
 *
 * Not thread-safe.
 */
class ConnectionsHolder {

    val connectionById = HashMap<UUID, ChatConnection>()

    fun putConnection(connection: ChatConnection) {
        connectionById[connection.id] = connection
    }

    fun removeConnection(connection: ChatConnection) {
        connectionById.remove(connection.id)
    }

    inline fun forEachConnection(action: (ChatConnection) -> Unit) {
        connectionById.forEach { (_, connection) ->
            action(connection)
        }
    }

    inline fun forConnection(connectionId: UUID, action: (ChatConnection) -> Unit) {
        connectionById[connectionId]?.let(action)
    }
}