package me.iloginovas.mychat.server.service.messaging

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import me.iloginovas.mychat.common.FastClearArrayBuffer
import me.iloginovas.mychat.common.SuspendingQueue
import me.iloginovas.mychat.server.data.MessageStorage
import me.iloginovas.mychat.server.data.TransactionManager
import me.iloginovas.mychat.server.data.UserStorage
import me.iloginovas.mychat.server.service.ChatEvent
import me.iloginovas.mychat.server.service.ClientEvents.SendMessage
import me.iloginovas.mychat.server.service.RequestEvents
import me.iloginovas.mychat.server.service.ServerEvent
import me.iloginovas.mychat.server.service.messaging.ChatEngineEvent.ClientConnectionClosed
import me.iloginovas.mychat.server.service.messaging.ChatEngineEvent.NewClientConnection
import me.iloginovas.mychat.server.service.messaging.handler.*
import org.slf4j.LoggerFactory
import java.util.*

private const val INCOMING_EVENT_BUFFER_SIZE = 100
private const val OUTGOING_EVENT_BUFFER_SIZE = 100

data class ChatConnection(
    val id: UUID = UUID.randomUUID(),
    val sessionId: String,
    val userId: Int,
    val serverEvents: Channel<ServerEvent> =
        Channel(
            OUTGOING_EVENT_BUFFER_SIZE,
            BufferOverflow.DROP_OLDEST
        )
)

sealed class ChatEngineEvent : ChatEvent {
    class NewClientConnection(val connection: ChatConnection) : ChatEngineEvent()
    class ClientConnectionClosed(val connection: ChatConnection) : ChatEngineEvent()
}

class ChatEngine(
    private val messageStorage: MessageStorage,
    private val userStorage: UserStorage,
    private val transactionManager: TransactionManager
) {
    private val log = LoggerFactory.getLogger(ChatEngine::class.java)

    private val connectionsHolder = ConnectionsHolder()
    private val eventQueue = SuspendingQueue<ChatEvent>(INCOMING_EVENT_BUFFER_SIZE)
    private val eventHandlers = EventHandlers()

    init {
        eventHandlers.apply {
            put(ClientConnectionClosedEventHandler(connectionsHolder))
            put(SendMessageEventHandler(connectionsHolder, messageStorage, transactionManager))
            put(EventHandlerNewClientConnection(connectionsHolder, messageStorage, transactionManager))
            put(GetMessagesRequestHandler(connectionsHolder, messageStorage, transactionManager))
            put(GetUserRequestHandler(connectionsHolder, userStorage, transactionManager))
        }
    }

    suspend fun postEvent(event: ChatEvent) {
        log.debug("Post event: $event")
        eventQueue.put(event)
    }

    fun start(coroutineScope: CoroutineScope) {
        coroutineScope.launch(Dispatchers.IO) {
            eventHandlers.forEach { it.init() }

            val buffer = FastClearArrayBuffer<ChatEvent>(INCOMING_EVENT_BUFFER_SIZE)
            while (isActive) {
                try {
                    eventQueue.drainTo(buffer, maxElements = INCOMING_EVENT_BUFFER_SIZE)
                    handleEvents(buffer)
                    buffer.clear()
                } catch (e: Throwable) {
                    ensureActive() // throws exception if coroutine is not active
                    log.error("Exception", e)
                }
            }
        }
    }

    private suspend fun handleEvents(events: Collection<ChatEvent>) {
        val connectionClosed = eventHandlers.get<ClientConnectionClosed>()
            .startHandling()
        val newMessages: EventCollector<SendMessage> = eventHandlers.get<SendMessage>()
            .startHandling(events.size)
        val newClientConnections = eventHandlers.get<NewClientConnection>()
            .startHandling()
        val getUserRequests = eventHandlers.get<RequestEvents.GetUsers>()
            .startHandling()
        val getMessagesRequests = eventHandlers.get<RequestEvents.GetMessages>()
            .startHandling()

        events.forEach { event ->
            when (event) {
                is ClientConnectionClosed -> connectionClosed.add(event)
                is SendMessage -> newMessages.add(event)
                is NewClientConnection -> newClientConnections.add(event)
                is RequestEvents.GetUsers -> getUserRequests.add(event)
                is RequestEvents.GetMessages -> getMessagesRequests.add(event)
            }
        }

        connectionClosed.handle()
        getUserRequests.handle()
        newMessages.handle()
        getMessagesRequests.handle()
        newClientConnections.handle()
    }
}