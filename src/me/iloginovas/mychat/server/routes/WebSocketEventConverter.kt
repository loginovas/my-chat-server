package me.iloginovas.mychat.server.routes

import com.google.gson.GsonBuilder
import me.iloginovas.mychat.server.model.NewMessage
import me.iloginovas.mychat.server.service.*
import me.iloginovas.mychat.server.webapi.websocket.*
import java.util.*

class WebSocketEventConverter {

    private val gson = GsonBuilder()
        .registerTypeAdapter(EventContainer::class.java, EventContainerGsonDeserializer())
        .setPrettyPrinting()
        .create()

    fun serialize(event: ServerEvent): String {
        val wsEvent = event.convertFromModel()
        val eventName = EventRegistry.getEventName(wsEvent::class)
        val eventContainer = EventContainer(wsEvent, eventName)
        return gson.toJson(eventContainer)
    }

    fun deserialize(json: String, connectionId: UUID, userId: Int): ClientEvent {
        val eventContainer = gson.fromJson(json, EventContainer::class.java)
        val wsEvent = eventContainer.event
        return wsEvent.convertToModel(connectionId, userId)
    }

    private fun WebSocketEvent.convertToModel(connectionId: UUID, userId: Int): ClientEvent =
        when (this) {
            is WsClientEvents.SendMessage ->
                ClientEvents.SendMessage(NewMessage(text, userId))
            is WsRequests.GetUsers ->
                RequestEvents.GetUsers(
                    RequestResponseEvent.Params(id, connectionId), userIds
                )
            is WsRequests.GetMessages.Latest ->
                RequestEvents.GetMessages.Latest(
                    RequestResponseEvent.Params(id, connectionId), preferredMaxCount
                )
            is WsRequests.GetMessages.OlderThan ->
                RequestEvents.GetMessages.OlderThan(
                    RequestResponseEvent.Params(id, connectionId), messageId, preferredMaxCount
                )
            is WsRequests.GetMessages.NewerThan ->
                RequestEvents.GetMessages.NewerThan(
                    RequestResponseEvent.Params(id, connectionId), messageId, preferredMaxCount
                )
            else ->
                throw IllegalStateException("Event ${this::class} is not supported")
        }

    private fun ServerEvent.convertFromModel(): WsServerEvent =
        when (this) {
            is ServerEvents.NewMessages ->
                WsServerEvents.NewMessages(messages)
            is ResponseEvents.GetUsers ->
                WsResponses.GetUsers(id, users.fromModel())
            is ResponseEvents.GetMessages ->
                WsResponses.GetMessages(id, messages)
            else ->
                throw IllegalStateException("Event ${this::class} is not supported")
        }
}

