package me.iloginovas.mychat.server.routes

import com.google.gson.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import io.ktor.util.*
import io.ktor.websocket.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import me.iloginovas.mychat.common.resultOrError
import me.iloginovas.mychat.server.DependencyInjection.Companion.getInstance
import me.iloginovas.mychat.server.model.ChatSession
import me.iloginovas.mychat.server.service.*
import org.slf4j.LoggerFactory
import java.util.*

private val sessionAttrKey = AttributeKey<ChatSession>("ChatSessionAttr")
private val eventConverter = WebSocketEventConverter()

fun Routing.chatWebSocket() = route("/ws") {
    val log = LoggerFactory.getLogger("ChatWebSocket")

    val sessionManager: SessionManager = application.getInstance()
    val chatService: ChatService = application.getInstance()

    /* Check that user is logged-in */
    intercept(ApplicationCallPipeline.ApplicationPhase.Features) {
        val sessionId = call.request.queryParameters["sessionId"] ?: kotlin.run {
            call.respond(HttpStatusCode.Unauthorized)
            finish()
            return@intercept
        }
        val session: ChatSession = sessionManager.getSessionById(sessionId)
            .resultOrError { errorMsg ->
                call.respond(HttpStatusCode.Unauthorized, errorMsg.orEmpty())
                finish()
                return@intercept
            }
        call.attributes.put(sessionAttrKey, session)
    }

    webSocket {
        val chatSession = call.attributes.getOrNull(sessionAttrKey) ?: kotlin.run {
            close(CloseReason(CloseReason.Codes.CANNOT_ACCEPT, "Authentication error"))
            return@webSocket
        }

        val connectionHandler = WebSocketConnectionHandler(
            this, chatSession, chatService
        )
        connectionHandler.handle()
    }
}

private class WebSocketConnectionHandler(
    private val webSocketSession: DefaultWebSocketServerSession,
    private val chatSession: ChatSession,
    private val chatService: ChatService,
) {
    private val log = LoggerFactory.getLogger(this::class.java)

    val scope = CoroutineScope(
        webSocketSession.coroutineContext +
                Job(webSocketSession.coroutineContext.job)
    )

    val connectionId: UUID = UUID.randomUUID()

    suspend fun handle() {
        val serverEventFlow: Flow<ServerEvent> = chatService.connectClient(
            connectionId, chatSession.sessionId, clientEventFlow
        )

        withContext(scope.coroutineContext) {
            serverEventFlow.collect { event: ServerEvent ->
                if (event is ServerEvents.ClientIsDisconnected)
                    scope.cancel(event.cause.orEmpty())
                try {
                    val json: String = eventConverter.serialize(event)
                    webSocketSession.send(json)
                } catch (e: Throwable) {
                    log.error("Send event error", e)
                }
            }
        }
    }

    private val clientEventFlow: Flow<ClientEvent> =
        flow {
            while (true) {
                val frame = try {
                    webSocketSession.incoming.receive()
                } catch (e: ClosedReceiveChannelException) {
                    log.debug("WebSocketSession should be closed")
                    scope.cancel()
                    return@flow
                }

                if (frame !is Frame.Text) continue

                try {
                    val event: ClientEvent = eventConverter.deserialize(
                        frame.readText(), connectionId, chatSession.userId
                    )
                    emit(event)
                } catch (e: Throwable) {
                    log.error("Receive event error", e)
                }
            }
        }
}