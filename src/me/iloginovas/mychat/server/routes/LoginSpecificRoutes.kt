package me.iloginovas.mychat.server.routes

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import me.iloginovas.mychat.common.resultOrError
import me.iloginovas.mychat.server.*
import me.iloginovas.mychat.server.DependencyInjection.Companion.getInstance
import me.iloginovas.mychat.server.model.ChatSession
import me.iloginovas.mychat.server.model.Credentials
import me.iloginovas.mychat.server.model.User
import me.iloginovas.mychat.server.service.SessionManager
import me.iloginovas.mychat.server.service.UserService
import me.iloginovas.mychat.server.webapi.WebApi
import me.iloginovas.mychat.server.webapi.WebApi.LoginResponse


fun Routing.loginRelatedRoutes() {
    val userService = application.getInstance<UserService>()
    val sessionManager = application.getInstance<SessionManager>()

    post("/user/login") {
        val credentials: Credentials =
            call.receive<WebApi.UserCredentials>().let {
                Credentials(it.username, it.password)
            }

        val user: User = userService.authenticate(credentials)
            .resultOrError { errorMsg ->
                val response = LoginResponse(successful = false, errorMsg = errorMsg)
                call.respond(HttpStatusCode.Unauthorized, response)
                return@post
            }

        val session: ChatSession = sessionManager.newSession(user)
            .resultOrError { errorMsg ->
                val response = LoginResponse(successful = false, errorMsg = errorMsg)
                call.respond(HttpStatusCode.Unauthorized, response)
                return@post
            }

        call.sessions.set(Session(session.sessionId))
        val response = LoginResponse(
            successful = true,
            sessionId = session.sessionId,
            user = WebApi.User(user.id, user.name)
        )
        call.respond(response)
    }

    post("/user/logout") {
        call.sessions.get<Session>()?.let {
            sessionManager.dropSession(it.sessionId)
            call.sessions.clear<Session>()
        }
        call.respond(HttpStatusCode.OK)
    }

    post("/user/registration") {
        val credentials: Credentials =
            call.receive<WebApi.UserCredentials>().let {
                Credentials(it.username, it.password)
            }

        val user: User = userService.registerUser(credentials)
            .resultOrError { errorMsg ->
                val response = LoginResponse(successful = false, errorMsg = errorMsg)
                call.respond(HttpStatusCode.Unauthorized, response)
                return@post
            }

        val session: ChatSession = sessionManager.newSession(user)
            .resultOrError { errorMsg ->
                val response = LoginResponse(successful = false, errorMsg = errorMsg)
                call.respond(HttpStatusCode.Unauthorized, response)
                return@post
            }

        call.sessions.set(Session(session.sessionId))
        val response = LoginResponse(
            successful = true,
            sessionId = session.sessionId,
            user = WebApi.User(user.id, user.name)
        )
        call.respond(response)
    }
}