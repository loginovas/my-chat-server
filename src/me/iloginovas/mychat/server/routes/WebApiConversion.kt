package me.iloginovas.mychat.server.routes

import me.iloginovas.mychat.server.model.User
import me.iloginovas.mychat.server.webapi.WebApi

fun User.fromModel(): WebApi.User =
    WebApi.User(id, name)

fun List<User>.fromModel(): List<WebApi.User> =
    map { it.fromModel() }