package me.iloginovas.mychat.server

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.ktor.application.*
import io.ktor.util.*
import me.iloginovas.mychat.server.data.*
import me.iloginovas.mychat.server.data.db.ChatDatabase
import me.iloginovas.mychat.server.data.db.TransactionManagerImpl
import me.iloginovas.mychat.server.service.*
import me.iloginovas.mychat.server.service.messaging.ChatServiceImpl

fun MutableMap<Class<*>, Any>.defineInstances() {
    ChatDatabase.init()

    singleton<Gson> {
        GsonBuilder().applyDefault().create()
    }

    /* Data */
    singleton<TransactionManager>(TransactionManagerImpl())
    singleton<UserStorage>(UserStorageImpl())
    singleton<SessionStorage>(SessionStorageImpl())
    singleton<MessageStorage>(MessageStorageImpl())

    /* Services */
    singleton<UserService>(
        UserServiceImpl(getInstance(), getInstance())
    )
    singleton<SessionManager>(
        SessionManagerImpl(getInstance(), getInstance(), getInstance())
    )
    singleton<ChatService>(
        ChatServiceImpl(getInstance(), getInstance(), getInstance(), getInstance())
    )
}

class DependencyInjection {
    private val classToInstanceMap: Map<Class<*>, Any>

    init {
        val map = HashMap<Class<*>, Any>()
        map.defineInstances()
        classToInstanceMap = map
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> getInstance(cls: Class<T>): T =
        classToInstanceMap[cls] as? T
            ?: error("Instance 'cls' not found")

    companion object {
        private val dependencyInjectionAttrKey =
            AttributeKey<DependencyInjection>("DependencyInjection")

        fun setup(app: Application, di: DependencyInjection = DependencyInjection()) {
            app.attributes.put(dependencyInjectionAttrKey, di)
        }

        inline fun <reified T : Any> Application.getInstance(): T {
            return this.getInstance(T::class.java)
        }

        fun <T : Any> Application.getInstance(cls: Class<T>): T =
            getDI().getInstance(cls)


        private fun Application.getDI(): DependencyInjection =
            attributes[dependencyInjectionAttrKey]
    }
}

private inline fun <reified T : Any> MutableMap<Class<*>, Any>.singleton(instance: T) {
    put(T::class.java, instance)
}

private inline fun <reified T : Any> MutableMap<Class<*>, Any>.singleton(
    instanceProvider: () -> T
) {
    put(T::class.java, instanceProvider())
}

private inline fun <reified T : Any> Map<Class<*>, Any>.getInstance(): T {
    return get(T::class.java) as T
}

