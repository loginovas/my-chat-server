package me.iloginovas.mychat.server

import com.google.gson.GsonBuilder
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import io.ktor.websocket.*
import me.iloginovas.mychat.server.DependencyInjection.Companion.getInstance
import me.iloginovas.mychat.server.routes.chatWebSocket
import me.iloginovas.mychat.server.routes.loginRelatedRoutes
import me.iloginovas.mychat.server.service.ChatService
import org.slf4j.event.Level
import java.text.DateFormat
import java.time.Duration
import java.util.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

data class Session(val sessionId: String)

@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    DependencyInjection.setup(this)
    getInstance<ChatService>().apply {
        start(this@module)

//        runBlocking {
//            val transactionManager = getInstance<TransactionManager>()
//            val user: User = transactionManager.readTransaction {
//                getInstance<UserStorage>().findByName("admin")!!
//            }
//            (1..500).forEach {
//                sendMessage(NewMessage("$it", user.id))
//            }
//        }
    }

    install(CallLogging) { configureCallLogging() }
    install(DefaultHeaders)
    install(ContentNegotiation) { configureContentNegotiation() }
    install(Sessions) { header<Session>("SESSION") }
    install(WebSockets) { configureWebSocket() }

    routing {
        loginRelatedRoutes()
        chatWebSocket()

        get("/") {
            call.respondText("HELLO WORLD3!", contentType = ContentType.Text.Plain)
        }

        webSocket("/myws/echo") {
            send(Frame.Text("Hi from server"))
            while (true) {
                val frame = incoming.receive()
                if (frame is Frame.Text) {
                    frame.readText()
                    send(Frame.Text("Client said: " + frame.readText()))
                }
            }
        }

        static("/static") {
            resources("static")
        }

        get("/json/gson") {
            call.respond(mapOf("hello" to "world"))
        }
    }
}

private fun ContentNegotiation.Configuration.configureContentNegotiation() {
    gson { applyDefault() }
}

fun GsonBuilder.applyDefault(): GsonBuilder {
    setDateFormat(DateFormat.LONG)
    setPrettyPrinting()
    return this
}

fun WebSockets.WebSocketOptions.configureWebSocket() {
    pingPeriod = Duration.ofSeconds(30)
    timeout = Duration.ofSeconds(15)
    maxFrameSize = Long.MAX_VALUE
    masking = false
}

fun CallLogging.Configuration.configureCallLogging() {
    level = Level.INFO
}

