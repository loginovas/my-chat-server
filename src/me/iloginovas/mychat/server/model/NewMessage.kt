package me.iloginovas.mychat.server.model

data class NewMessage(
    val text: String,

    /** Sender user id */
    val senderId: Int,
)