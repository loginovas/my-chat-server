package me.iloginovas.mychat.server.model

data class User(
    val id: Int,

    /** @see me.iloginovas.mychat.common.validation.UsernameValidator **/
    val name: String,

    /** @see me.iloginovas.mychat.common.validation.PasswordValidator **/
    val password: String
)

data class NewUser(

    /** @see me.iloginovas.mychat.common.validation.UsernameValidator **/
    val name: String,

    /** @see me.iloginovas.mychat.common.validation.PasswordValidator **/
    val password: String
)