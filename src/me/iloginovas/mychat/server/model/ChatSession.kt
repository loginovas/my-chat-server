package me.iloginovas.mychat.server.model

data class ChatSession(
    val sessionId: String,
    val userId: Int
)