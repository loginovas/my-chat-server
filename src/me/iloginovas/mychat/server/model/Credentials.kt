package me.iloginovas.mychat.server.model

data class Credentials(
    val username: String,
    val password: String
)