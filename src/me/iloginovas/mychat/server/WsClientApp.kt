package me.iloginovas.mychat.server

import io.ktor.application.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.websocket.*
import io.ktor.client.features.websocket.WebSockets
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.utils.io.*
import io.ktor.websocket.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import io.ktor.websocket.WebSockets.Feature as ServerWebSocket

object WsClientApp {

    @JvmStatic
    fun main(args: Array<String>) {
        runBlocking {
            val port = 8081

            val server = embeddedServer(Netty, host = "localhost", port = port) {
                install(ServerWebSocket)
                routing {
                    webSocket("/echo") {
                        send(Frame.Text("Welcome!"))
                        for (frame in incoming) {
                            frame as? Frame.Text ?: continue
                            val receivedText = frame.readText()
                            send("You said: $receivedText")
                        }
                    }
                }
            }.start()

            coroutineContext.job.invokeOnCompletion {
                server.stop(3000, 3000)
            }

            val client = HttpClient(CIO).config { install(WebSockets) }
            client.ws(method = HttpMethod.Get, host = "localhost", port = port, path = "/echo") {
                launch(Dispatchers.IO) {
                    while (true) {
                        when (val str = readLine()) {
                            null -> continue
                            "stop" -> {
                                server.stop(3000, 3000)
                                return@launch
                            }
                            else -> send(Frame.Text(str))
                        }
                    }
                }
                launch {
                    send(Frame.Text("Hello"))

                    for (message in incoming) {
                        message as? Frame.Text ?: continue
                        println("Server said: " + message.readText())
                    }
                }
            }
            println("AFTER WEB SOCKET")
        }
    }
}

//    val client = HttpClient(CIO) {
//        install(HttpTimeout) {}
//        install(Auth) {
//
//        }
//        install(JsonFeature) {
//            serializer = GsonSerializer()
//        }
//        install(Logging) {
//            level = LogLevel.HEADERS
//        }
//    }
//    runBlocking {
//        // Sample for making a HTTP Client request
//
//        val message = client.post<JsonSampleClass> {
//            url("http://127.0.0.1:8080/path/to/endpoint")
//            contentType(ContentType.Application.Json)
//            body = JsonSampleClass(hello = "world")
//        }
//    }

//data class JsonSampleClass(val hello: String)
