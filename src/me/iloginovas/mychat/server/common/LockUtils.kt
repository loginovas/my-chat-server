package me.iloginovas.mychat.server.common

import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.produceIn
import java.util.concurrent.locks.Lock

inline fun <T> Lock.lock(action: () -> T): T {
    lock()
    try {
        return action()
    } finally {
        unlock()
    }
}

inline fun <T> lock(l1: Lock, l2: Lock, action: () -> T): T {
    return l1.lock {
        l2.lock(action)
    }
}

fun <T> lock(vararg locks: Lock, action: () -> T): T {
    if (locks.isEmpty()) return action()

    val lock = locks.first()
    lock.lock {
        val otherLocks = when {
            locks.size > 1 -> locks.sliceArray(1 until locks.size)
            else -> emptyArray()
        }
        return lock(*otherLocks, action = action)
    }
}

fun <T : Any> Flow<T>.bufferedChunks(maxChunkSize: Int): Flow<List<T>> {
    require(maxChunkSize >= 1)
    return flow<List<T>> {
        coroutineScope {
            val upstreamChannel: ReceiveChannel<T> = this@bufferedChunks.buffer(maxChunkSize).produceIn(this)
            while (true) { // loop until closed
                val bufferChunks = ArrayList<T>(maxChunkSize) // allocate new array list every time
                // receive the first element (suspend until it is there)
                // null here means the channel was closed -> terminate the outer loop
                val first = upstreamChannel.receiveOrNull() ?: break
                bufferChunks.add(first)
                while (bufferChunks.size < maxChunkSize) {
                    // poll subsequent elements from the channel's buffer without waiting while they are present
                    // null here means there are no more element or channel was closed -> break from this loop
                    val element = upstreamChannel.poll() ?: break
                    bufferChunks.add(element)
                }
                emit(bufferChunks)
            }
        }
    }
}