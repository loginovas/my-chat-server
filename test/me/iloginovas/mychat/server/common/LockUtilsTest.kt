package me.iloginovas.mychat.server.me.iloginovas.mychat.server.common

import me.iloginovas.mychat.server.common.lock
import me.iloginovas.mychat.server.me.iloginovas.mychat.server.mock
import org.junit.Test
import org.mockito.Mockito.*
import java.util.concurrent.locks.Lock
import kotlin.test.assertEquals

class LockUtilsTest {

    @Test
    fun testWithZeroLocks() {
        val result = lock { 5 }
        assertEquals(5, result)
    }

    @Test
    fun testWithOneLock() {
        val lock: Lock = mock()

        val result = lock(lock) {
            verify(lock).lock()
            verify(lock, never()).unlock()
            verifyNoMoreInteractions(lock)
            5
        }

        verify(lock).lock()
        verify(lock).unlock()
        verifyNoMoreInteractions(lock)
        assertEquals(5, result)
    }

    @Test
    fun testWithTwoLock() {
        val lock1: Lock = mock()
        val lock2: Lock = mock()
        val inOrder = inOrder(lock1, lock2)

        val result = lock(lock1, lock2) {
            inOrder.verify(lock1).lock()
            inOrder.verify(lock2).lock()
            inOrder.verifyNoMoreInteractions()
            5
        }

        inOrder.verify(lock2).unlock()
        inOrder.verify(lock1).unlock()
        inOrder.verifyNoMoreInteractions()
        assertEquals(5, result)
    }

    @Test
    fun testWithThreeLock() {
        val lock1: Lock = mock()
        val lock2: Lock = mock()
        val lock3: Lock = mock()
        val inOrder = inOrder(lock1, lock2, lock3)

        val result = lock(lock1, lock2, lock3) {
            inOrder.verify(lock1).lock()
            inOrder.verify(lock2).lock()
            inOrder.verify(lock3).lock()
            inOrder.verifyNoMoreInteractions()
            5
        }

        inOrder.verify(lock3).unlock()
        inOrder.verify(lock2).unlock()
        inOrder.verify(lock1).unlock()
        inOrder.verifyNoMoreInteractions()
        assertEquals(5, result)
    }
}