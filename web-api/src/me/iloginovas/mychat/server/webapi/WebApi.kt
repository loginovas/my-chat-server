package me.iloginovas.mychat.server.webapi

object WebApi {

    data class UserCredentials(val username: String, val password: String)

    data class User(val id: Int, val name: String)

    data class LoginResponse(
        val successful: Boolean,
        val sessionId: String? = null,
        val user: User? = null,
        val errorMsg: String? = null
    )

    data class NewMessage(val text: String)
}