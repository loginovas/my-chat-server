package me.iloginovas.mychat.server.webapi.websocket

import java.util.*

interface WebSocketEvent

interface WsClientEvent : WebSocketEvent
interface WsServerEvent : WebSocketEvent

interface WsRequestResponse : WebSocketEvent {
    val id: UUID
}

interface WsRequest<T : WsResponse> :
    WsRequestResponse, WsClientEvent

interface WsResponse :
    WsRequestResponse, WsServerEvent

open class WsErrorResponse(
    override val id: UUID
) : WsResponse
