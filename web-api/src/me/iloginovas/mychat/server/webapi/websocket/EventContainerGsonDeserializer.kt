package me.iloginovas.mychat.server.webapi.websocket

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class EventContainerGsonDeserializer : JsonDeserializer<EventContainer> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): EventContainer {
        val jsonObject = json.asJsonObject
        val eventName = jsonObject.getAsJsonPrimitive(EventContainer::eventName.name).asString
        val eventClass = EventRegistry.getEventClass<WebSocketEvent>(eventName)

        val eventJson = jsonObject.get(EventContainer::event.name)
        val event: WebSocketEvent = context.deserialize(eventJson, eventClass.java)
        return EventContainer(event, eventName)
    }

}