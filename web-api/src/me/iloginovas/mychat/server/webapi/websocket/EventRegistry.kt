package me.iloginovas.mychat.server.webapi.websocket

import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
object EventRegistry {
    private val eventByName = HashMap<String, KClass<*>>()
    private val nameByEvent = HashMap<KClass<*>, String>()

    fun <T : WebSocketEvent> getEventClass(name: String): KClass<T> {
        return eventByName[name] as KClass<T>
    }

    fun <T : WebSocketEvent> getEventName(eventClass: KClass<T>): String {
        return nameByEvent[eventClass]!!
    }

    init {
        register(WsClientEvents.SendMessage::class)
        register(WsServerEvents.NewMessages::class)

        register(WsErrorResponse::class)

        register(WsRequests.GetUsers::class)
        register(WsResponses.GetUsers::class)

        register(WsRequests.GetMessages.Latest::class)
        register(WsRequests.GetMessages.NewerThan::class)
        register(WsRequests.GetMessages.OlderThan::class)
        register(WsResponses.GetMessages::class)
    }

    private fun <T : WebSocketEvent> register(eventClass: KClass<T>) {
        val name = eventClass.qualifiedName!!
        eventByName[name] = eventClass
        nameByEvent[eventClass] = name
    }
}