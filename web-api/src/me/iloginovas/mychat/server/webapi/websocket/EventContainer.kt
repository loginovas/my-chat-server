package me.iloginovas.mychat.server.webapi.websocket

class EventContainer(
    val event: WebSocketEvent,
    val eventName: String = EventRegistry.getEventName(event::class)
)

