package me.iloginovas.mychat.server.webapi.websocket

import me.iloginovas.mychat.common.model.Message
import me.iloginovas.mychat.server.webapi.WebApi
import java.util.*

class WsClientEvents {
    class SendMessage(val text: String) : WsClientEvent
}

class WsServerEvents {
    class NewMessages(val messages: List<Message>) : WsServerEvent
}

class WsRequests {

    class GetUsers(
        override val id: UUID = UUID.randomUUID(),
        val userIds: List<Int>
    ) : WsRequest<WsResponses.GetUsers>

    sealed class GetMessages : WsRequest<WsResponses.GetMessages> {
        class Latest(
            override val id: UUID = UUID.randomUUID(), val preferredMaxCount: Int
        ) : GetMessages()

        class NewerThan(
            override val id: UUID = UUID.randomUUID(), val messageId: Int, val preferredMaxCount: Int
        ) : GetMessages()

        class OlderThan(
            override val id: UUID = UUID.randomUUID(), val messageId: Int, val preferredMaxCount: Int
        ) : GetMessages()
    }
}

class WsResponses {
    class GetUsers(override val id: UUID, val users: List<WebApi.User>) : WsResponse
    class GetMessages(override val id: UUID, val messages: List<Message>) : WsResponse
}